package com.sweet.simple.login.entity;

import cn.hutool.core.date.DatePattern;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.extension.activerecord.Model;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 玩家账号代币券
 *
 * @author 大师兄
 */
@Setter
@Getter
@TableName("taiwan_billing.cash_cera_point")
public class CashCeraPoint extends Model<CashCeraPoint> {

    /**
     * 玩家账号, uId
     */
    @TableId(type = IdType.NONE)
    private String account;

    /**
     * Q点数量
     */
    private Integer ceraPoint;

    /**
     * 账号注册时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = DatePattern.NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private Date regDate;

    /**
     * 最后修改账号信息的时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = DatePattern.NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private Date modDate;
}
