package com.sweet.simple.login.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sweet.simple.login.entity.MemberWhiteAccount;
import com.sweet.simple.login.mapper.MemberWhiteAccountMapper;
import org.springframework.stereotype.Service;

/**
 * 游戏玩家白名单账户信息 服务实现类
 *
 * @author 大师兄
 */
@Service
public class MemberWhiteAccountService extends ServiceImpl<MemberWhiteAccountMapper, MemberWhiteAccount> {

}
