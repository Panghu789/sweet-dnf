package com.sweet.simple.login;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.context.annotation.ComponentScan;

@MapperScan("com.sweet.simple.login.mapper")
@SpringBootApplication(scanBasePackages = {"com.sweet.simple.login", "cn.hutool.extra.spring"})
public class SimpleLoginApplication {

    public static void main(String[] args) {
        new SpringApplicationBuilder(SimpleLoginApplication.class).headless(false).run(args);
        ViewStart.run(args);
    }
}
