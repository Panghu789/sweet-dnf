package com.sweet.simple.login.entity;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

/**
 * 存储玩家的游戏选项设置
 *
 * @author 大师兄
 */
@Setter
@Getter
@TableName("taiwan_login.member_game_option")
public class MemberGameOption {

    /**
     * 账号id
     */
    private Integer mId;

    /**
     * 游戏选项1
     */
    @TableField("option_1")
    private Byte[] option1;

    /**
     * 游戏选项2
     */
    @TableField("option_2")
    private Byte[] option2;

    /**
     * 游戏选项3
     */
    @TableField("option_3")
    private Byte[] option3;

    /**
     * 快捷表情
     */
    private Byte[] shortcutEmoticon;
}
