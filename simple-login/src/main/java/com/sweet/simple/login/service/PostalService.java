package com.sweet.simple.login.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sweet.simple.login.entity.Postal;
import com.sweet.simple.login.mapper.PostalMapper;
import org.springframework.stereotype.Service;

/**
 * @Author: [ph]
 * @Date: 2024/6/3 22:58
 * @Description: 邮件表具体业务实现
 **/
@Service
public class PostalService extends ServiceImpl<PostalMapper, Postal> {
}
