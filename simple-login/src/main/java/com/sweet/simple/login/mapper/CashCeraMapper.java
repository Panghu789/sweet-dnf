package com.sweet.simple.login.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sweet.simple.login.entity.CashCera;

/**
 * 玩家账号点劵 Mapper
 *
 * @author 大师兄
 */
public interface CashCeraMapper extends BaseMapper<CashCera> {

}
