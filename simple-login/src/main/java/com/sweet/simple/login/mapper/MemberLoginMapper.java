package com.sweet.simple.login.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sweet.simple.login.entity.MemberLogin;

/**
 *  游戏玩家的登录信息 Mapper 接口
 *
 * @author 大师兄
 */
public interface MemberLoginMapper extends BaseMapper<MemberLogin> {

}
