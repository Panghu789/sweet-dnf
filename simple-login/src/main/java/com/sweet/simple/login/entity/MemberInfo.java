package com.sweet.simple.login.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * 游戏玩家的个人信息的表
 *
 * @author 大师兄
 */
@Setter
@Getter
@NoArgsConstructor
@TableName("d_taiwan.member_info")
public class MemberInfo {

    /**
     * 账号id
     */
    @TableId(value = "m_id", type = IdType.AUTO)
    private Integer mId;

    /**
     * 玩家的用户ID
     */
    private String userId;

    /**
     * 玩家的用户名
     */
    private String userName;

    /**
     * 玩家的身份证号码（加密存储）的第一部分
     */
    private String firstSsn;

    /**
     * 玩家的身份证号码（加密存储）的第二部分
     */
    private String secondSsn;

    /**
     * 玩家的密码（加密存储）
     */
    private String passwd;

    /**
     * 玩家的手机号码
     */
    private String mobileNo;

    /**
     * 玩家注册的日期
     */
    private Integer regDate;

    /**
     * 玩家的电子邮件地址
     */
    private String email;

    /**
     * 玩家设置的密保问题编号
     */
    private Integer qNo;

    /**
     * 玩家设置的密保问题答案（加密存储）
     */
    private String qAnswer;

    /**
     * 玩家信息最近一次更新的日期
     */
    private LocalDateTime updtDate;

    /**
     * 玩家账户状态
     */
    private Integer state;

    /**
     * 玩家的昵称
     */
    private String nickname;

    /**
     * 玩家是否接收邮件的标记
     */
    private String emailYn;

    /**
     * 玩家身份证验证的标记
     */
    private Integer ssnCheck;

    /**
     * 玩家的角色槽位数量
     */
    private Integer slot;

    /**
     * 玩家最近一次游戏的时间
     */
    private LocalDateTime lastPlayTime;

    /**
     * 玩家是否是Hangame会员的标记
     */
    private Integer hangameFlag;

    /**
     * 玩家是否是Hanmon会员的标记
     */
    private Integer hanmonFlag;

    /**
     * 玩家账户类型
     */
    private Integer mType;

    public MemberInfo(Integer mId, String userId) {
        this.mId = mId;
        this.userId = userId;
    }
}
