package com.sweet.simple.login.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sweet.simple.login.entity.LimitCreateCharacter;
import com.sweet.simple.login.mapper.LimitCreateCharacterMapper;
import org.springframework.stereotype.Service;

/**
 * 游戏玩家创建角色的限制信息 服务实现类
 *
 * @author 大师兄
 */
@Service
public class LimitCreateCharacterService extends ServiceImpl<LimitCreateCharacterMapper, LimitCreateCharacter> {

}
