package com.sweet.simple.login.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sweet.simple.login.entity.MemberGameOption;
import com.sweet.simple.login.mapper.MemberGameOptionMapper;
import org.springframework.stereotype.Service;

/**
 * 存储玩家的游戏选项设置 服务类
 *
 * @author 大师兄
 */
@Service
public class MemberGameOptionService extends ServiceImpl<MemberGameOptionMapper, MemberGameOption> {

}
