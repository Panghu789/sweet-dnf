package com.sweet.simple.login.entity;

import cn.hutool.core.date.DatePattern;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Getter;
import lombok.Setter;
import org.springframework.format.annotation.DateTimeFormat;

import java.util.Date;

/**
 * 玩家账号点劵
 *
 * @author 大师兄
 */
@Setter
@Getter
@TableName("taiwan_billing.cash_cera")
public class CashCera {

    /**
     * 玩家账号, uId
     */
    @TableId(type = IdType.NONE)
    private String account;

    /**
     * 点劵数量
     */
    private Integer cera;

    /**
     * 最后修改账号信息的交易流水号
     */
    private Long modTran;

    /**
     * 最后修改账号信息的时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = DatePattern.NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private Date modDate;

    /**
     * 账号注册时间
     */
    @JsonFormat(timezone = "GMT+8", pattern = DatePattern.NORM_DATETIME_PATTERN)
    @DateTimeFormat(pattern = DatePattern.NORM_DATETIME_PATTERN)
    private Date regDate;
}
