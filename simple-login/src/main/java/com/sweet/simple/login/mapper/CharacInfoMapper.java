package com.sweet.simple.login.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sweet.simple.login.entity.CharacInfo;

public interface CharacInfoMapper extends BaseMapper<CharacInfo> {

}
