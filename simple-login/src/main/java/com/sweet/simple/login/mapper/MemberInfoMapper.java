package com.sweet.simple.login.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sweet.simple.login.entity.MemberInfo;

/**
 * 游戏玩家的个人信息的表 Mapper 接口
 *
 * @author 大师兄
 */
public interface MemberInfoMapper extends BaseMapper<MemberInfo> {

}
