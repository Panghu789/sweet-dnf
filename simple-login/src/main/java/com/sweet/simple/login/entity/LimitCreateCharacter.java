package com.sweet.simple.login.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 *
 * 游戏玩家创建角色的限制信息
 *
 * @author 大师兄
 */
@Setter
@Getter
@NoArgsConstructor
@TableName("d_taiwan.limit_create_character")
public class LimitCreateCharacter {

    /**
     * 账号id
     */
    @TableId(type = IdType.NONE)
    private Integer mId;

    /**
     * 该玩家已经创建的角色数量
     */
    private Integer count;

    /**
     * 该玩家最近一次创建的时间
     */
    private LocalDateTime lastAccessTime;

    public LimitCreateCharacter(Integer mId) {
        this.mId = mId;
    }
}
