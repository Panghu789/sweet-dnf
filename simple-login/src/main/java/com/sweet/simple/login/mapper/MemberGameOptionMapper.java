package com.sweet.simple.login.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sweet.simple.login.entity.MemberGameOption;

/**
 *  存储玩家的游戏选项设置 Mapper 接口
 *
 * @author 大师兄
 */
public interface MemberGameOptionMapper extends BaseMapper<MemberGameOption> {

}
