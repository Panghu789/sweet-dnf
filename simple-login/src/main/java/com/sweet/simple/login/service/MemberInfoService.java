package com.sweet.simple.login.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sweet.simple.login.entity.MemberInfo;
import com.sweet.simple.login.mapper.MemberInfoMapper;
import org.springframework.stereotype.Service;

/**
 * 游戏玩家的个人信息的表 服务实现类
 *
 * @author 大师兄
 */
@Service
public class MemberInfoService extends ServiceImpl<MemberInfoMapper, MemberInfo> {

}
