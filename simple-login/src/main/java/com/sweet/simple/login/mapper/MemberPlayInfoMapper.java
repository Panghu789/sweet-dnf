package com.sweet.simple.login.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sweet.simple.login.entity.MemberPlayInfo;

/**
 *  游戏玩家的游戏信息 Mapper 接口
 *
 * @author 大师兄
 */
public interface MemberPlayInfoMapper extends BaseMapper<MemberPlayInfo> {

}
