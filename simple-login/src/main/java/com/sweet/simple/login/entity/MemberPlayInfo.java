package com.sweet.simple.login.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;

/**
 * 游戏玩家的游戏信息
 *
 * @author 大师兄
 */
@Setter
@Getter
@TableName("taiwan_login.member_play_info")
public class MemberPlayInfo {

    /**
     * 游戏记录发生的日期。
     */
    private LocalDate occDate;

    /**
     * 玩家的唯一标识符。
     */
    private Integer mId;

    /**
     * 玩家在当天的游戏时间。
     */
    private Integer playTime;

    /**
     * 玩家在当天的游戏次数。
     */
    private Integer playCount;

    /**
     * 玩家在当天的交易次数。
     */
    private Integer tradeCnt;

    /**
     * 玩家在当天获得的经验值。
     */
    private Long exp;

    /**
     * 玩家在当天使用的疲劳值。
     */
    private Integer usedFatigue;

    /**
     * 玩家登录时使用的IP地址。
     */
    private String ip;

    /**
     * 玩家最近一次游戏的时间。
     */
    private Integer lastPlayTime;

    /**
     * 玩家是否在PCBang网吧中游戏的标记。
     */
    private Integer pcbangFlag;

    /**
     * 玩家退出游戏时使用的IP地址。
     */
    private String endIp;

    /**
     * 玩家在当天的听说次数。
     */
    private Integer tingCount;

    /**
     * 玩家登录时使用的MAC地址。
     */
    private String macAddr;

    /**
     * 玩家所在的服务器ID。
     */
    private Integer serverId;
}
