package com.sweet.simple.login.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sweet.simple.login.entity.MemberLogin;
import com.sweet.simple.login.mapper.MemberLoginMapper;
import org.springframework.stereotype.Service;

/**
 * 游戏玩家的登录信息 服务实现类
 *
 * @author 大师兄
 */
@Service
public class MemberLoginService extends ServiceImpl<MemberLoginMapper, MemberLogin> {

}
