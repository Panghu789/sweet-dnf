package com.sweet.simple.login.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.time.LocalDateTime;

/**
 * 游戏玩家白名单账户信息
 *
 * @author 大师兄
 */
@Setter
@Getter
@TableName("d_taiwan.member_white_account")
@NoArgsConstructor
public class MemberWhiteAccount {

    /**
     * 账号id
     */
    private Integer mId;

    /**
     * 玩家被加入白名单的日期
     */
    private LocalDateTime regDate;

    public MemberWhiteAccount(Integer mId) {
        this.mId = mId;
    }
}
