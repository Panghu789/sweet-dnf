package com.sweet.simple.login.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sweet.simple.login.entity.Postal;

public interface PostalMapper extends BaseMapper<Postal> {
}
