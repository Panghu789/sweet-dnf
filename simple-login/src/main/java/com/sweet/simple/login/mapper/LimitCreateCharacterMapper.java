package com.sweet.simple.login.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sweet.simple.login.entity.LimitCreateCharacter;

/**
 * 游戏玩家创建角色的限制信息 Mapper 接口
 *
 * @author 大师兄
 */
public interface LimitCreateCharacterMapper extends BaseMapper<LimitCreateCharacter> {

}
