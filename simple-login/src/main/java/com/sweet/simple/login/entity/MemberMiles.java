package com.sweet.simple.login.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 游戏玩家里程数信息
 *
 * @author 大师兄
 */
@Setter
@Getter
@NoArgsConstructor
@TableName("d_taiwan.member_miles")
public class MemberMiles {

    /**
     * 账号id
     */
    @TableId(type = IdType.NONE)
    private Integer mId;

    /**
     * 玩家当前的总里程数
     */
    private Integer miles;

    /**
     * 玩家今日已获得的里程数
     */
    private Integer dailyMiles;

    public MemberMiles(Integer mId) {
        this.mId = mId;
    }
}
