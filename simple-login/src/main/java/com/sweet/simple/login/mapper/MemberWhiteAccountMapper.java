package com.sweet.simple.login.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sweet.simple.login.entity.MemberWhiteAccount;

/**
 * 游戏玩家白名单账户信息 Mapper 接口
 *
 * @author 大师兄
 */
public interface MemberWhiteAccountMapper extends BaseMapper<MemberWhiteAccount> {

}
