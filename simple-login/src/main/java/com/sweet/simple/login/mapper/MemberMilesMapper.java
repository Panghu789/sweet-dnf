package com.sweet.simple.login.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sweet.simple.login.entity.MemberMiles;

/**
 * 游戏玩家里程数信息 Mapper 接口
 *
 * @author 大师兄
 */
public interface MemberMilesMapper extends BaseMapper<MemberMiles> {

}
