package com.sweet.simple.login.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sweet.simple.login.entity.CdkManagement;
import com.sweet.simple.login.mapper.CdkManagementMapper;
import org.springframework.stereotype.Service;

/**
 * @Author: [ph]
 * @Date: 2024/6/3 22:58
 * @Description: cdk表具体业务实现
 **/
@Service
public class CdkManagementService extends ServiceImpl<CdkManagementMapper, CdkManagement> {

    /**
     * @Author 「ph」
     * @Description 修改cdk状态为已使用
     * @param: cdkCode - [String] cdk码
     * @return: boolean
     * @Version 1.0.0
     * @Date 2024/06/02 09:31
     */
    public boolean editStatus(String cdkCode) {
        return lambdaUpdate().eq(CdkManagement::getCdkCode, cdkCode)
                .set(CdkManagement::getUseFlag, 1).update();
    }
}
