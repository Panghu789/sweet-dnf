package com.sweet.simple.login.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sweet.simple.login.entity.MemberJoinInfo;
import com.sweet.simple.login.mapper.MemberJoinInfoMapper;
import org.springframework.stereotype.Service;

/**
 * 游戏玩家注册信息 服务实现类
 *
 * @author 大师兄
 */
@Service
public class MemberJoinInfoService extends ServiceImpl<MemberJoinInfoMapper, MemberJoinInfo> {

}
