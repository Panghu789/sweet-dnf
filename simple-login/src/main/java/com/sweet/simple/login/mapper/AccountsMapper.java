package com.sweet.simple.login.mapper;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sweet.simple.login.entity.Accounts;

public interface AccountsMapper extends BaseMapper<Accounts> {

}
