package com.sweet.simple.login.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sweet.simple.login.entity.MemberMiles;
import com.sweet.simple.login.mapper.MemberMilesMapper;
import org.springframework.stereotype.Service;

/**
 * 游戏玩家里程数信息 服务实现类
 *
 * @author 大师兄
 */
@Service
public class MemberMilesService extends ServiceImpl<MemberMilesMapper, MemberMiles> {

}
