package com.sweet.simple.login.entity;

import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.Setter;

/**
 * 游戏玩家的登录信息
 *
 * @author 大师兄
 */
@Setter
@Getter
@TableName("taiwan_login.member_login")
public class MemberLogin {

    /**
     * 角色编号
     */
    private Integer mId;

    /**
     * 玩家最近一次登录的时间
     */
    private Integer loginTime;

    /**
     * 通常为一段时间后自动退出登录
     */
    private Integer expireTime;

    /**
     * 玩家登录的过期时间，通常为一段时间后自动退出登录
     */
    private Integer lastPlayTime;

    /**
     * 该账户总共登录失败的次数
     */
    private Integer totalAccountFail;

    /**
     * 该账户最近一次登录失败的次数
     */
    private Integer accountFail;

    /**
     * 玩家被举报的次数
     */
    private Integer reportCnt;

    /**
     * 玩家的可靠性标记，用于判断该玩家是否可信
     */
    private Integer reliableFlag;

    /**
     * 玩家每天可以交易的金币数量
     */
    private Integer tradeGoldDaily;

    /**
     * 玩家最近一次领取礼物的时间
     */
    private Integer lastGiftTime;

    /**
     * 玩家已经领取的礼物数量
     */
    private Integer giftCnt;

    /**
     * 玩家登录时使用的IP地址
     */
    private String loginIp;

    /**
     * 玩家的安全标记，用于判断该玩家是否存在安全问题
     */
    private Integer securityFlag;

    /**
     * 玩家所属的阵营
     */
    private Integer powerSide;

    /**
     * 玩家在副本中获得的金币数量
     */
    private Integer dungeonGainGold;

    /**
     * 玩家所属的学派
     */
    private Integer schoolId;

    /**
     * 玩家的评级
     */
    private Double rating;

    /**
     * 玩家在清洗装备时获得的点数
     */
    private Integer cleanpadPoint;

    /**
     * 玩家是否可以跳过新手教程
     */
    private String tutorialSkipable;

    /**
     * 玩家参加活动时的标记
     */
    private Integer eventCharacFlag;

    /**
     * Garena游戏平台的令牌密钥
     */
    private Long garenaTokenKey;

}

