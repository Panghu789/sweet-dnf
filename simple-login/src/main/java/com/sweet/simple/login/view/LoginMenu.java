package com.sweet.simple.login.view;

import cn.hutool.core.convert.Convert;
import cn.hutool.core.io.FileUtil;
import cn.hutool.core.io.resource.ResourceUtil;
import cn.hutool.core.lang.Tuple;
import cn.hutool.core.util.CharsetUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.extra.spring.SpringUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.sweet.simple.login.entity.*;
import com.sweet.simple.login.service.*;
import com.sweet.simple.login.util.Dialog;
import com.sweet.simple.login.util.RsaUtil;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;

import javax.swing.*;
import java.awt.*;
import java.io.File;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.time.LocalDate;
import java.util.Date;

/**
 * 游戏登录
 *
 * @author 大师兄
 */
@Slf4j
public class LoginMenu {

    /**
     * 游戏启动目录键
     */
    private final static String GAME_PATH_KEY = "key_game_path";

    public static void build(JMenuBar menuBar) {
        JMenu loginMenu = new JMenu();
        loginMenu.setText("游戏登录");

        JMenuItem startGamePathMenuItem = new JMenuItem();
        startGamePathMenuItem.setText("设置游戏目录");
        startGamePathMenuItem.addActionListener(e -> startGamePathMenuItemAction());
        loginMenu.add(startGamePathMenuItem);

        JMenuItem loginMenuItem = new JMenuItem();
        loginMenuItem.setText("登录游戏");
        loginMenuItem.addActionListener(e -> loginMenuItemAction());
        loginMenu.add(loginMenuItem);

        JMenuItem registerMenuItem = new JMenuItem();
        registerMenuItem.setText("账号注册");
        registerMenuItem.addActionListener(e -> registerMenuItemAction());
        loginMenu.add(registerMenuItem);

        // 生成秘钥对，暂时放在帮助菜单下 TODO
        JMenuItem createSecretKeyMenuItem = new JMenuItem();
        createSecretKeyMenuItem.setText("生成密钥对");
        createSecretKeyMenuItem.addActionListener(e -> createSecretKeyMenuItemAction());
        loginMenu.add(createSecretKeyMenuItem);

        menuBar.add(loginMenu);
    }

    /**
     * 生成秘钥对操作方法，文件生成以后放到桌面
     * 格式化秘钥 TODO
     */
    private static void createSecretKeyMenuItemAction() {
        Tuple tuple = RsaUtil.createSecretKeyPair(true, true);
        File userHOmeDir = FileUtil.getUserHomeDir();
        // 用户桌面路径
        String desktopPath = userHOmeDir.getAbsolutePath() + "\\Desktop\\";
        FileUtil.writeUtf8String(tuple.get(0), desktopPath + "privatekey.pem");
        FileUtil.writeUtf8String(tuple.get(1), desktopPath + "publickey.pem");
        // 成功提示
        Dialog dialog = new Dialog();
        dialog.success("生成成功");
    }

    private static void loginMenuItemAction() {
        String gamePath = SweetPreferences.getState().get(GAME_PATH_KEY, null);
        if (StrUtil.isBlank(gamePath)) {
            error("还未设置启动游戏程序(DNF.exe)，请先设置");
            return;
        }

        JLabel jLabel = new JLabel();
        jLabel.setText("登录游戏");

        JOptionPane customOptionPane = new JOptionPane();
        JTextField jTextField = new JFormattedTextField();

        JButton submitButton = new JButton("登录");
        submitButton.addActionListener(e -> submitLogin(jTextField.getText()));

        customOptionPane.setMessage(new Object[]{jTextField});
        customOptionPane.setOptions(new Object[]{submitButton});
        JOptionPane.showOptionDialog(null, customOptionPane.getMessage(), jLabel.getText(), customOptionPane.getOptionType(), customOptionPane.getMessageType(), customOptionPane.getIcon(), customOptionPane.getOptions(), customOptionPane.getInitialValue());
    }

    /**
     * 游戏登录
     *
     * @param username 账号
     */
    @SneakyThrows
    private static void submitLogin(String username) {
        if (StrUtil.isBlank(username)) {
            error("账号不能为空");
            return;
        }
        AccountsService accountsService = SpringUtil.getBean(AccountsService.class);
        Accounts accounts = accountsService.getOne(new LambdaQueryWrapper<Accounts>().eq(Accounts::getAccountName, username));
        if (accounts == null) {
            error("账号不存在");
            return;
        }
        String privateKey = ResourceUtil.readStr("privatekey.pem", CharsetUtil.CHARSET_UTF_8);
        String loginParams = RsaUtil.secureId(privateKey, accounts.getUId());
        String runCmd = "start DNF.exe " + loginParams;
        log.info(runCmd);
        // 创建一个ProcessBuilder对象，指定要执行的命令
        ProcessBuilder processBuilder = new ProcessBuilder("cmd.exe", "/c", runCmd);
        // 设置命令执行的工作目录
        String gamePath = SweetPreferences.getState().get(GAME_PATH_KEY, null);
        processBuilder.directory(new File(gamePath));
        // 启动进程并执行命令
        processBuilder.start();
    }

    /**
     * 账号注册操作
     */
    private static void registerMenuItemAction() {
        JLabel jLabel = new JLabel("账号注册");

        JOptionPane customOptionPane = new JOptionPane();

        JTextField jTextField = new JTextField(20);
        JPasswordField jPasswordField = new JPasswordField(20);

        JButton submitButton = new JButton("注册");
        submitButton.addActionListener(e -> submitRegister(jTextField.getText(), new String(jPasswordField.getPassword()), customOptionPane));

        // 使用GridBagLayout
        GridBagLayout gridBagLayout = new GridBagLayout();
        JPanel panel = new JPanel(gridBagLayout);
        GridBagConstraints constraints = new GridBagConstraints();
        constraints.insets = new Insets(5, 5, 5, 5); // 设置组件之间的间距

        // 添加标签和输入框
        constraints.gridx = 0;
        constraints.gridy = 0;
        constraints.anchor = GridBagConstraints.WEST;
        panel.add(new JLabel("用户名: "), constraints);

        constraints.gridx = 1;
        panel.add(jTextField, constraints);

        constraints.gridx = 0;
        constraints.gridy = 1;
        panel.add(new JLabel("密码: "), constraints);

        constraints.gridx = 1;
        panel.add(jPasswordField, constraints);

        // 添加注册按钮
        constraints.gridx = 0;
        constraints.gridy = 2;
        constraints.gridwidth = 2;
        constraints.anchor = GridBagConstraints.CENTER;
        panel.add(submitButton, constraints);

        customOptionPane.setMessage(panel); // 将 panel 设置为消息

        customOptionPane.setOptions(new Object[]{});
        JOptionPane.showOptionDialog(null, customOptionPane.getMessage(), jLabel.getText(), customOptionPane.getOptionType(), customOptionPane.getMessageType(), customOptionPane.getIcon(), customOptionPane.getOptions(), customOptionPane.getInitialValue());

    }

    /**
     * 账号注册
     *
     * @param username 账号
     */
    @SneakyThrows
    private static void submitRegister(String username, String password, JOptionPane optionPane) {
        MemberGameOptionService memberGameOptionService = SpringUtil.getBean(MemberGameOptionService.class);
        MemberPlayInfoService memberPlayInfoService = SpringUtil.getBean(MemberPlayInfoService.class);
        CashCeraService cashCeraService = SpringUtil.getBean(CashCeraService.class);
        CashCeraPointService cashCeraPointService = SpringUtil.getBean(CashCeraPointService.class);
        MemberLoginService memberLoginService = SpringUtil.getBean(MemberLoginService.class);
        MemberWhiteAccountService memberWhiteAccountService = SpringUtil.getBean(MemberWhiteAccountService.class);
        MemberMilesService memberMilesService = SpringUtil.getBean(MemberMilesService.class);
        MemberJoinInfoService memberJoinInfoService = SpringUtil.getBean(MemberJoinInfoService.class);
        MemberInfoService memberInfoService = SpringUtil.getBean(MemberInfoService.class);
        LimitCreateCharacterService limitCreateCharacterService = SpringUtil.getBean(LimitCreateCharacterService.class);
        if (StrUtil.isBlank(username)) {
            error("账号不能为空");
            return;
        }
        if (StrUtil.isBlank(password)) {
            error("密码不能为空");
            return;
        }
        AccountsService accountsService = SpringUtil.getBean(AccountsService.class);
        Accounts accounts = accountsService.getOne(new LambdaQueryWrapper<Accounts>().eq(Accounts::getAccountName, username));
        if (accounts != null) {
            error("账号已经存在");
            return;
        }
        accounts = new Accounts();
        accounts.setAccountName(username);
        accounts.setPassword(toMd5(password));
        accounts.setVip(StrUtil.EMPTY);
        accountsService.save(accounts);

        // 游戏选项信息
        MemberGameOption memberGameOption = new MemberGameOption();
        memberGameOption.setMId(accounts.getUId());
        memberGameOption.setOption1(new Byte[]{});
        memberGameOption.setOption2(new Byte[]{});
        memberGameOption.setOption3(new Byte[]{});
        memberGameOption.setShortcutEmoticon(new Byte[]{});
        memberGameOptionService.save(memberGameOption);

        // 游戏信息
        MemberPlayInfo memberPlayInfo = new MemberPlayInfo();
        memberPlayInfo.setMId(accounts.getUId());
        memberPlayInfo.setOccDate(LocalDate.now());
        memberPlayInfo.setServerId(1);
        memberPlayInfoService.save(memberPlayInfo);

        // 点券信息
        CashCera cashCera = new CashCera();
        cashCera.setAccount(Convert.toStr(accounts.getUId()));
        cashCera.setCera(0);
        cashCera.setModDate(new Date());
        cashCera.setRegDate(new Date());
        cashCera.setModTran(0L);
        cashCeraService.save(cashCera);

        // 代币券信息
        CashCeraPoint cashCeraPoint = new CashCeraPoint();
        cashCeraPoint.setAccount(Convert.toStr(accounts.getUId()));
        cashCeraPoint.setCeraPoint(0);
        cashCeraPoint.setRegDate(new Date());
        cashCeraPoint.setModDate(new Date());
        cashCeraPointService.save(cashCeraPoint);

        // 登录信息
        MemberLogin memberLogin = new MemberLogin();
        memberLogin.setMId(accounts.getUId());
        memberLoginService.save(memberLogin);

        memberWhiteAccountService.save(new MemberWhiteAccount(accounts.getUId()));
        memberMilesService.save(new MemberMiles(accounts.getUId()));
        memberJoinInfoService.save(new MemberJoinInfo(accounts.getUId()));
        memberInfoService.save(new MemberInfo(accounts.getUId(), Convert.toStr(accounts.getUId())));
        limitCreateCharacterService.save(new LimitCreateCharacter(accounts.getUId()));

        // 关闭对话框 无效 TODO
        Window window = SwingUtilities.getWindowAncestor(optionPane);
        if (window != null) {
            window.dispose();
        }
    }

    /**
     * 设置游戏目录操作
     */
    private static void startGamePathMenuItemAction() {
        JLabel jLabel = new JLabel();
        jLabel.setText("设置游戏目录");
        JButton jButton = new JButton("选择文件");
        // 触发事件
        String gamePath = SweetPreferences.getState().get(GAME_PATH_KEY, null);
        // 设置初始值
        JTextField jTextField = new JTextField(gamePath);
        jButton.addActionListener(e -> chooseStartGame(jTextField));
        JOptionPane customOptionPane = new JOptionPane();
        customOptionPane.setMessage(new Object[]{jButton, jTextField});
        JOptionPane.showOptionDialog(null, customOptionPane.getMessage(), jLabel.getText(), customOptionPane.getOptionType(), customOptionPane.getMessageType(), customOptionPane.getIcon(), customOptionPane.getOptions(), customOptionPane.getInitialValue());
    }

    /**
     * 选择游戏目录操作
     */
    private static void chooseStartGame(JTextField jTextField) {
        JFileChooser chooser = new JFileChooser();
        int result = chooser.showOpenDialog(null);
        if (result == JFileChooser.APPROVE_OPTION) {
            File selectedFile = chooser.getSelectedFile();
            String filePath = selectedFile.getAbsolutePath();
            filePath = StrUtil.removeSuffix(filePath, "DNF.exe");
            // 保存设置
            jTextField.setText(filePath);
            SweetPreferences.getState().put(GAME_PATH_KEY, filePath);
        }
    }

    /**
     * 通过弹窗显示错误的提示信息
     *
     * @param message 提示内容
     */
    private static void error(String message) {
        JOptionPane.showMessageDialog(null, message, "提示", JOptionPane.ERROR_MESSAGE);
    }

    /**
     * 输入一个字符串，返回其MD5哈希值的十六进制表示。具体来说，
     * 该方法使用Java标准库中的java.security.MessageDigest类来计算输入字符串的MD5哈希值。
     * 首先，通过调用getInstance("MD5")方法获取一个MD5哈希对象md。然后，使用md.update()方法将输入字符串转换为字节数组，
     * 并将其写入哈希对象中。接下来，通过调用md.digest()方法获取哈希对象的MD5哈希值，并将其转换为十六进制表示的字符串。
     * 最终，toMd5方法返回输入字符串的MD5哈希值的十六进制表示。
     *
     * @param password 原密码
     * @return 加密以后的密码
     */
    private static String toMd5(String password) {
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
            md.update(password.getBytes());
            byte[] digest = md.digest();
            StringBuilder sb = new StringBuilder();
            for (byte b : digest) {
                sb.append(String.format("%02x", b & 0xff));
            }
            return sb.toString();
        }
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException("MD5 algorithm not found");
        }
    }

    public static void main(String[] args) {

    }
}
