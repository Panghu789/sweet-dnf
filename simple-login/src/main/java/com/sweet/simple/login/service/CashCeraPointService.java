package com.sweet.simple.login.service;


import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sweet.simple.login.entity.CashCeraPoint;
import com.sweet.simple.login.mapper.CashCeraPointMapper;
import org.springframework.stereotype.Service;

/**
 * 玩家账号代币券 服务实现
 *
 * @author 大师兄
 */
@Service
public class CashCeraPointService extends ServiceImpl<CashCeraPointMapper, CashCeraPoint> {

}
