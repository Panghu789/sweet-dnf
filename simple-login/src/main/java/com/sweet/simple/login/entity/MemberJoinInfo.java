package com.sweet.simple.login.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * 游戏玩家注册信息
 *
 * @author 大师兄
 */
@Setter
@Getter
@NoArgsConstructor
@TableName("d_taiwan.member_join_info")
public class MemberJoinInfo {

    /**
     * 账号id
     */
    @TableId(type = IdType.NONE)
    private Integer mId;

    /**
     * 注册日期
     */
    private Integer regDate;

    /**
     * 玩家注册时使用的IP地址
     */
    private String ip;

    /**
     * 玩家注册时所在的国家代码
     */
    private Integer contryCode;

    /**
     * 玩家最近一次登录的时间
     */
    private Integer loginTime;

    /**
     * 玩家最近一次登录失败的类型
     */
    private Integer errorType;

    /**
     * 玩家最近一次登录时使用的IP地址
     */
    private String loginIp;

    /**
     * 玩家游戏使用历史记录
     */
    private Integer gameUseHistory;

    public MemberJoinInfo(Integer mId) {
        this.mId = mId;
    }
}
