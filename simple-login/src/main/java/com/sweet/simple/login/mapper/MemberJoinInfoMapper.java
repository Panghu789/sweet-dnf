package com.sweet.simple.login.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sweet.simple.login.entity.MemberJoinInfo;

/**
 * 游戏玩家注册信息 Mapper 接口
 *
 * @author 大师兄
 */
public interface MemberJoinInfoMapper extends BaseMapper<MemberJoinInfo> {

}
