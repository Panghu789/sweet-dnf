package com.sweet.simple.login.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sweet.simple.login.entity.MemberPlayInfo;
import com.sweet.simple.login.mapper.MemberPlayInfoMapper;
import org.springframework.stereotype.Service;

/**
 * 游戏玩家的游戏信息 服务实现类
 *
 * @author 大师兄
 */
@Service
public class MemberPlayInfoService extends ServiceImpl<MemberPlayInfoMapper, MemberPlayInfo> {

}
