package com.sweet.simple.login.service;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.sweet.simple.login.entity.CashCera;
import com.sweet.simple.login.mapper.CashCeraMapper;
import org.springframework.stereotype.Service;

/**
 * 玩家账号点劵 服务实现
 *
 * @author 大师兄
 */
@Service
public class CashCeraService extends ServiceImpl<CashCeraMapper, CashCera> {

}
