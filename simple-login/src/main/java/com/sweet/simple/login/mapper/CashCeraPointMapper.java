package com.sweet.simple.login.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.sweet.simple.login.entity.CashCeraPoint;

/**
 * 玩家账号Q点 Mapper
 *
 * @author 大师兄
 */
public interface CashCeraPointMapper extends BaseMapper<CashCeraPoint> {

}
