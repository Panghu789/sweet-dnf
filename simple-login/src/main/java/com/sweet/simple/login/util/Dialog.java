package com.sweet.simple.login.util;

import cn.hutool.core.util.ObjectUtil;
import lombok.Getter;
import lombok.Setter;

import javax.swing.*;
import java.awt.*;

/**
 * 弹窗组件
 *
 * @author 大师兄
 */
@Setter
@Getter
public class Dialog extends JLabel {

    /**
     * 弹窗标题对象
     */
    private JLabel titleLabel;

    /**
     * 弹窗面板对象
     */
    private JOptionPane optionPane;

    /**
     * 显示成功弹窗
     *
     * @param message 文字信息
     */
    public void success(String message) {
        success(null, message);
    }

    /**
     * 显示成功弹窗
     *
     * @param title   标题
     * @param message 文字信息
     */
    public void success(String title, String message) {
        title = ObjectUtil.defaultIfBlank(title, "提示");
        titleLabel = new JLabel(title);
        optionPane = new JOptionPane(message);
        showDialog();
    }

    /**
     * 显示错误弹窗
     *
     * @param message 文字信息
     */
    public void error(String message) {
        error(null, message);
    }

    /**
     * 显示错误弹窗
     *
     * @param title   标题
     * @param message 文字信息
     */
    public void error(String title, String message) {
        title = ObjectUtil.defaultIfBlank(title, "提示");
        titleLabel = new JLabel(title);
        optionPane = new JOptionPane(message);
        optionPane.setMessageType(JOptionPane.ERROR_MESSAGE);
        showDialog();
    }

    /**
     * 自定义弹窗
     *
     * @param titleLabel titleLabel
     * @param optionPane optionPane
     */
    public void custom(JLabel titleLabel, JOptionPane optionPane) {
        this.titleLabel = titleLabel;
        this.optionPane = optionPane;
        showDialog();
    }

    /**
     * 显示弹窗
     */
    private void showDialog() {
        Window window = SwingUtilities.windowForComponent(this);
        if (optionPane.getWantsInput())
            JOptionPane.showInputDialog(window, optionPane.getMessage(), titleLabel.getText(), optionPane.getMessageType(), optionPane.getIcon(), null, null);
        else
            JOptionPane.showOptionDialog(window, optionPane.getMessage(), titleLabel.getText(), optionPane.getOptionType(), optionPane.getMessageType(), optionPane.getIcon(), optionPane.getOptions(), optionPane.getInitialValue());

    }
}