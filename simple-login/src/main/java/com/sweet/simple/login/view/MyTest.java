/*
 * Created by JFormDesigner on Mon Jun 03 16:46:20 CST 2024
 */

package com.sweet.simple.login.view;

import javax.swing.*;
import net.miginfocom.swing.*;

/**
 * @author 76789
 */
public class MyTest extends JPanel {
    public MyTest() {
        initComponents();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents  @formatter:off
        label1 = new JLabel();
        textField1 = new JTextField();
        label2 = new JLabel();
        textField2 = new JTextField();
        button1 = new JButton();

        //======== this ========
        setLayout(new MigLayout(
            "hidemode 3",
            // columns
            "[fill]" +
            "[fill]",
            // rows
            "[]" +
            "[]" +
            "[]"));

        //---- label1 ----
        label1.setText("\u8d26\u53f7");
        add(label1, "cell 0 0,wmin 50px");
        add(textField1, "cell 1 0,wmin 150px");

        //---- label2 ----
        label2.setText("\u5bc6\u7801");
        add(label2, "cell 0 1,wmin 50px");
        add(textField2, "cell 1 1,wmin 150px");

        //---- button1 ----
        button1.setText("\u767b\u5f55");
        add(button1, "cell 1 2");
        // JFormDesigner - End of component initialization  //GEN-END:initComponents  @formatter:on
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables  @formatter:off
    private JLabel label1;
    private JTextField textField1;
    private JLabel label2;
    private JTextField textField2;
    private JButton button1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables  @formatter:on
}
